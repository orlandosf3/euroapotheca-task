<?php declare(strict_types=1);

use App\Factory\AnalyzerFactory;
use App\Factory\FinderFactory;
use App\Factory\OutputFactory;
use App\Factory\SentenceParserFactory;
use App\Service\Analyzer;
use App\Service\SentenceParser;

require_once(__DIR__ . '/vendor/autoload.php');

$analyzer       = AnalyzerFactory::createNew(Analyzer::class);
$sentenceParser = SentenceParserFactory::createNew(SentenceParser::class);

$invalid = false;
// gets arguments from the CLI
$arguments = $_SERVER['argv'];

$validations = require_once(__DIR__ . '/config/validations.php');

// this is very primitive validation logic for the specific solution
foreach ($validations as $index => $validation) {
    if (isset($validation['defined']) && empty($arguments[$index])) {
        fputs(STDERR, $validation['defined'] . PHP_EOL);
        $invalid = true;
    } elseif (isset($validation['options']) && ! in_array($arguments[$index], $validation['options']['in'])) {
        fputs(STDERR, $validation['options']['message'] . PHP_EOL);
        $invalid = true;
    } elseif (isset($validation['path']) && ! realpath($arguments[$index])) {
        fputs(STDERR, $validation['path'] . PHP_EOL);
        $invalid = true;
    }
}

if (true === $invalid) {
    fputs(STDERR, 'Command format: php analyzer.php <file_path> <scope> <finder_type> <output_type>' . PHP_EOL);
    exit;
}

$map = require_once(__DIR__ . '/config/map.php');

$output = $analyzer->analyze(
    $sentenceParser->parse($arguments[1], '.?!', intval($arguments[2])),
    FinderFactory::createNew($map['finder'][intval($arguments[3])]),
    OutputFactory::createNew($map['output'][intval($arguments[4])])
);

fputs(STDOUT, '------ Results ------' . PHP_EOL);
fputs(STDOUT, implode(PHP_EOL . '----' . PHP_EOL, $output) . PHP_EOL);
fputs(STDOUT, '---------------------' . PHP_EOL);
