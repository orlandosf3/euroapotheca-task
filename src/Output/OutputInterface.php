<?php declare(strict_types=1);

namespace App\Output;

use App\DataSet\FinderResultSetInterface;
use App\DataSet\OutputSetInterface;

interface OutputInterface
{
    /**
     * @param FinderResultSetInterface $finderResultSet
     *
     * @return OutputSetInterface
     */
    public function process(FinderResultSetInterface $finderResultSet): OutputSetInterface;
}
