<?php declare(strict_types=1);

namespace App\Output;

use App\DataSet\FinderResultSetInterface;
use App\DataSet\OutputSetInterface;

class MaxCountWithCountOutput extends AbstractOutput
{
    use SortTrait;

    public const TYPE = 4;

    /**
     * @var int
     */
    protected $outputType = self::TYPE;

    /**
     * @param FinderResultSetInterface $finderResultSet
     * @param OutputSetInterface       $outputSet
     *
     * @return OutputSetInterface
     */
    protected function execute(FinderResultSetInterface $finderResultSet, OutputSetInterface $outputSet): OutputSetInterface
    {
        $items = $this->sortItemsDesc($finderResultSet);
        $item = reset($items);

        $outputSet->addItem(sprintf('%d | %s', $item->getCount(), $item->getValue()));

        return $outputSet;
    }
}
