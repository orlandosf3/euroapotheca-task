<?php declare(strict_types=1);

namespace App\Output;

use App\DataSet\FinderResultSet\Item;
use App\DataSet\FinderResultSetInterface;
use Closure;

trait SortTrait
{
    /**
     * @param FinderResultSetInterface $finderResultSet
     *
     * @return Item[]|array
     */
    private function sortItemsAsc(FinderResultSetInterface $finderResultSet): array
    {
        return $this->sortItems($finderResultSet, function (Item $itemA, Item $itemB) {
            return $itemA->getCount() - $itemB->getCount();
        });
    }

    /**
     * @param FinderResultSetInterface $finderResultSet
     *
     * @return Item[]|array
     */
    private function sortItemsDesc(FinderResultSetInterface $finderResultSet): array
    {
        return $this->sortItems($finderResultSet, function (Item $itemA, Item $itemB) {
            return $itemB->getCount() - $itemA->getCount();
        });
    }

    /**
     * @param FinderResultSetInterface $finderResultSet
     * @param Closure                  $callback
     *
     * @return Item[]|array
     */
    private function sortItems(FinderResultSetInterface $finderResultSet, Closure $callback): array
    {
        $items = [];

        if ($finderResultSet->isNotEmpty()) {
            $items = $finderResultSet->getItems();

            usort($items, $callback);
        }

        return array_values($items);
    }
}
