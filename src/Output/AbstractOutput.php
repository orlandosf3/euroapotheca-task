<?php declare(strict_types=1);

namespace App\Output;

use App\DataSet\FinderResultSetInterface;
use App\DataSet\OutputSet;
use App\DataSet\OutputSetInterface;

abstract class AbstractOutput implements OutputInterface
{
    /**
     * @var int
     */
    protected $outputType;

    public function process(FinderResultSetInterface $finderResultSet): OutputSetInterface
    {
        return $this->execute($finderResultSet, $this->createOutputSet());
    }

    /**
     * @return OutputSetInterface
     */
    protected function createOutputSet(): OutputSetInterface
    {
        return new OutputSet();
    }

    abstract protected function execute(
        FinderResultSetInterface $sentenceSet,
        OutputSetInterface $outputSet
    ): OutputSetInterface;
}
