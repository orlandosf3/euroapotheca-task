<?php declare(strict_types=1);

namespace App\Output;

use App\DataSet\FinderResultSetInterface;
use App\DataSet\OutputSetInterface;

class SortDescWithCountOutput extends AbstractOutput
{
    use SortTrait;

    public const TYPE = 8;

    /**
     * @var int
     */
    protected $outputType = self::TYPE;

    /**
     * @param FinderResultSetInterface $finderResultSet
     * @param OutputSetInterface       $outputSet
     *
     * @return OutputSetInterface
     */
    protected function execute(FinderResultSetInterface $finderResultSet, OutputSetInterface $outputSet): OutputSetInterface
    {
        $items = $this->sortItemsDesc($finderResultSet);

        foreach ($items as $item) {
            $outputSet->addItem(sprintf('%d | %s', $item->getCount(), $item->getValue()));
        }

        return $outputSet;
    }
}
