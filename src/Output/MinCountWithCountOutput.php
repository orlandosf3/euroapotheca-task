<?php declare(strict_types=1);

namespace App\Output;

use App\DataSet\FinderResultSetInterface;
use App\DataSet\OutputSetInterface;

class MinCountWithCountOutput extends AbstractOutput
{
    use SortTrait;

    public const TYPE = 2;

    /**
     * @var int
     */
    protected $outputType = self::TYPE;

    /**
     * @param FinderResultSetInterface $finderResultSet
     * @param OutputSetInterface       $outputSet
     *
     * @return OutputSetInterface
     */
    protected function execute(FinderResultSetInterface $finderResultSet, OutputSetInterface $outputSet): OutputSetInterface
    {
        $items = $this->sortItemsAsc($finderResultSet);
        $item = reset($items);

        $outputSet->addItem(sprintf('%d | %s', $item->getCount(), $item->getValue()));

        return $outputSet;
    }
}
