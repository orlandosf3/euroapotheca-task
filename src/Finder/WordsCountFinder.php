<?php declare(strict_types=1);

namespace App\Finder;

use App\DataSet\FinderResultSet\Item;
use App\DataSet\FinderResultSetInterface;
use App\DataSet\SentenceSetInterface;

final class WordsCountFinder extends AbstractFinder
{
    public const TYPE = 2;

    /**
     * @var int
     */
    protected $finderType = self::TYPE;

    /**
     * @param SentenceSetInterface     $sentenceSet
     * @param FinderResultSetInterface $finderResultSet
     *
     * @return FinderResultSetInterface
     */
    protected function execute(
        SentenceSetInterface $sentenceSet,
        FinderResultSetInterface $finderResultSet
    ): FinderResultSetInterface
    {
        if ($sentenceSet->isNotEmpty()) {
            foreach ($sentenceSet->getSentences() as $sentence) {
                $sentence = trim($sentence);

                $item = new Item();
                $item->setCount(str_word_count($sentence, 0));
                $item->setValue($sentence);

                $finderResultSet->addItem($item);
            }
        }

        return $finderResultSet;
    }
}
