<?php declare(strict_types=1);

namespace App\Finder;

use App\DataSet\FinderResultSet;
use App\DataSet\FinderResultSetInterface;
use App\DataSet\SentenceSetInterface;

abstract class AbstractFinder implements FinderInterface
{
    /**
     * @var int
     */
    protected $finderType;

    /**
     * @param SentenceSetInterface $sentenceSet
     *
     * @return FinderResultSetInterface
     */
    public function process(SentenceSetInterface $sentenceSet): FinderResultSetInterface
    {
        return $this->execute($sentenceSet, $this->createFinderResultSet());
    }

    /**
     * @return FinderResultSetInterface
     */
    protected function createFinderResultSet(): FinderResultSetInterface
    {
        $finderResultSet = new FinderResultSet();
        $finderResultSet->setType($this->finderType);

        return $finderResultSet;
    }

    /**
     * @param SentenceSetInterface     $sentenceSet
     * @param FinderResultSetInterface $finderResultSet
     *
     * @return FinderResultSetInterface
     */
    abstract protected function execute(
        SentenceSetInterface $sentenceSet,
        FinderResultSetInterface $finderResultSet
    ): FinderResultSetInterface;
}
