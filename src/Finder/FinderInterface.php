<?php declare(strict_types=1);

namespace App\Finder;

use App\DataSet\FinderResultSetInterface;
use App\DataSet\SentenceSetInterface;

interface FinderInterface
{
    /**
     * @param SentenceSetInterface $sentenceSet
     *
     * @return FinderResultSetInterface
     */
    public function process(SentenceSetInterface $sentenceSet): FinderResultSetInterface;
}
