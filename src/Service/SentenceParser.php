<?php declare(strict_types=1);

namespace App\Service;

use App\DataSet\SentenceSet;
use App\DataSet\SentenceSetInterface;

class SentenceParser implements SentenceParserInterface
{
    /**
     * It reads a file and finds all possible sentences using specific terminators.
     *
     * @param string $path
     * @param string $terminators
     * @param int    $scope
     *
     * @return SentenceSetInterface
     */
    public function parse(
        string $path,
        string $terminators = self::TERMINATORS,
        int $scope = self::SCOPE_FILE
    ): SentenceSetInterface {
        $resource = fopen($path, 'r');
        $set      = $this->createSet();

        // todo: update logic if we don't need a new line interpret as a end of the sentence
        while (false !== $line = fgets($resource)) {
            $set->addSentence($this->parseLine($line, $terminators));

            if ($scope === self::SCOPE_LINE) {
                break;
            }
        }

        return $set;
    }

    /**
     * @param string $line
     * @param string $terminators
     *
     * @return array
     */
    private function parseLine(string $line, string $terminators = self::TERMINATORS): array
    {
        // todo: update pattern if we need to find the end of the sentence which ends with the one of Terminators but doesn't have a space or a newline
        $pattern = sprintf('/(?<=[%s])\s+(?=[a-z])/i', $terminators);

        if ($line) {
            return array_filter(
                array_map(
                    function (string $sentence) {
                        return trim($sentence);
                    },
                    preg_split($pattern, $line)
                )
            );
        }

        return [];
    }

    /**
     * @return SentenceSetInterface
     */
    protected function createSet(): SentenceSetInterface
    {
        return new SentenceSet();
    }
}
