<?php declare(strict_types=1);

namespace App\Service;

use App\DataSet\SentenceSetInterface;

interface SentenceParserInterface
{
    public const TERMINATORS = '.?!';
    public const SCOPE_FILE  = 1;
    public const SCOPE_LINE  = 2;

    /**
     * @param string $path
     * @param string $terminators
     * @param int    $scope
     *
     * @return SentenceSetInterface
     */
    public function parse(
        string $path,
        string $terminators = self::TERMINATORS,
        int $scope = self::SCOPE_FILE
    ): SentenceSetInterface;
}
