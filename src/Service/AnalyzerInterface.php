<?php declare(strict_types=1);

namespace App\Service;

use App\DataSet\SentenceSetInterface;
use App\Finder\FinderInterface;
use App\Output\OutputInterface;

interface AnalyzerInterface
{
    /**
     * @param SentenceSetInterface $sentenceSet
     * @param FinderInterface      $finder
     * @param OutputInterface      $output
     *
     * @return array
     */
    public function analyze(SentenceSetInterface $sentenceSet, FinderInterface $finder, OutputInterface $output): array;
}
