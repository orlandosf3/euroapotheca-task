<?php declare(strict_types=1);

namespace App\Factory;

use App\Finder\FinderInterface;

class FinderFactory implements FactoryInterface
{
    public static function createNew(string $class): FinderInterface
    {
        return new $class();
    }
}
