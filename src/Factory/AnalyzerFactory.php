<?php declare(strict_types=1);

namespace App\Factory;

use App\Service\AnalyzerInterface;

class AnalyzerFactory implements FactoryInterface
{
    public static function createNew(string $class): AnalyzerInterface
    {
        return new $class();
    }
}
