<?php declare(strict_types=1);

namespace App\Factory;

use App\Output\OutputInterface;

class OutputFactory implements FactoryInterface
{
    public static function createNew(string $class): OutputInterface
    {
        return new $class();
    }
}
