<?php declare(strict_types=1);

namespace App\Factory;

interface FactoryInterface
{
    /**
     * @param string $class
     *
     * @return object
     */
    public static function createNew(string $class);
}
