<?php declare(strict_types=1);

namespace App\Factory;

use App\Service\SentenceParserInterface;

class SentenceParserFactory implements FactoryInterface
{
    public static function createNew(string $class): SentenceParserInterface
    {
        return new $class();
    }
}
