<?php declare(strict_types=1);

namespace App\DataSet;

class FinderResultSet implements FinderResultSetInterface
{
    /**
     * @var int
     */
    private $type;

    /**
     * @var FinderResultSet\Item[]|array
     */
    private $items = [];

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType(int $type): void
    {
        $this->type = $type;
    }

    /**
     * @return FinderResultSet\Item[]|array
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param FinderResultSet\Item[]|array $items
     */
    public function setItems(array $items): void
    {
        $this->items = $items;
    }

    /**
     * @param FinderResultSet\Item $item
     */
    public function addItem(FinderResultSet\Item $item): void
    {
        $this->items[] = $item;
    }

    /**
     * @return bool
     */
    public function isNotEmpty(): bool
    {
        return ! empty($this->items);
    }
}
