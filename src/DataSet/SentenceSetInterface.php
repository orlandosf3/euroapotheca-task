<?php declare(strict_types=1);

namespace App\DataSet;

interface SentenceSetInterface
{
    /**
     * @return array
     */
    public function getSentences(): array;

    /**
     * @param array $sentences
     */
    public function setSentences(array $sentences): void;

    /**
     * @param $sentence
     */
    public function addSentence($sentence): void;

    /**
     * @return bool
     */
    public function isNotEmpty(): bool;

    /**
     * @return int
     */
    public function count(): int;
}
