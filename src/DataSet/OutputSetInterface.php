<?php declare(strict_types=1);

namespace App\DataSet;

interface OutputSetInterface
{
    /**
     * @return int|null
     */
    public function getType(): ?int;

    /**
     * @param int|null $type
     */
    public function setType(?int $type): void;

    /**
     * @return array
     */
    public function getItems(): array;

    /**
     * @param array $items
     */
    public function setItems(array $items): void;

    /**
     * @param string $item
     */
    public function addItem(string $item): void;
}
