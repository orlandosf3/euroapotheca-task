<?php declare(strict_types=1);

namespace App\DataSet;

class SentenceSet implements SentenceSetInterface
{
    /**
     * @var array
     */
    private $sentences = [];

    /**
     * @return array
     */
    public function getSentences(): array
    {
        return $this->sentences;
    }

    /**
     * @param array $sentences
     */
    public function setSentences(array $sentences): void
    {
        $this->sentences = $sentences;
    }

    /**
     * @param $sentence
     */
    public function addSentence($sentence): void
    {
        if (is_array($sentence)) {
            $this->sentences = array_merge($this->sentences, $sentence);
        } else {
            $this->sentences[] = $sentence;
        }
    }

    public function isNotEmpty(): bool
    {
        return ! empty($this->sentences);
    }

    public function count(): int
    {
        return count($this->sentences);
    }
}
