<?php declare(strict_types=1);

namespace App\DataSet\FinderResultSet;

class Item
{
    /**
     * @var int
     */
    private $count = 0;

    /**
     * @var string|null
     */
    private $value;

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @param int $count
     */
    public function setCount(int $count): void
    {
        $this->count = $count;
    }

    /**
     * @return string|null
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @param string|null $value
     */
    public function setValue(?string $value): void
    {
        $this->value = $value;
    }
}
