<?php declare(strict_types=1);

namespace App\DataSet;

class OutputSet implements OutputSetInterface
{
    /**
     * @var int|null
     */
    private $type;

    /**
     * @var array
     */
    private $items = [];

    /**
     * @return int|null
     */
    public function getType(): ?int
    {
        return $this->type;
    }

    /**
     * @param int|null $type
     */
    public function setType(?int $type): void
    {
        $this->type = $type;
    }

    /**
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param array $items
     */
    public function setItems(array $items): void
    {
        $this->items = $items;
    }

    /**
     * @param string $item
     */
    public function addItem(string $item): void
    {
        $this->items[] = $item;
    }
}
