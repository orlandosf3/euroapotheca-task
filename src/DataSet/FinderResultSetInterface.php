<?php declare(strict_types=1);

namespace App\DataSet;

interface FinderResultSetInterface
{
    /**
     * @return int
     */
    public function getType(): int;

    /**
     * @param int $type
     */
    public function setType(int $type): void;

    /**
     * @return FinderResultSet\Item[]|array
     */
    public function getItems();

    /**
     * @param FinderResultSet\Item[]|array $items
     */
    public function setItems(array $items): void;

    /**
     * @param FinderResultSet\Item $item
     */
    public function addItem(FinderResultSet\Item $item): void;

    /**
     * @return bool
     */
    public function isNotEmpty(): bool;
}
