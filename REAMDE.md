# Text analyzer
The analyzer works everywhere where PHP v^7.3 is installed. It works as a CLI command with some required arguments.

## Start project
Installation:
* launch `php composer.phar install`

Run tests:
* Unix: `./vendor/bin/phpunit`
* Windows: `.\vendor\bin\phpunit`

## Usage instruction
The analyzer command format: `php analyzer.php <file_path> <scope> <finder_type> <output_type>`

### Available arguments options
* `<scope>`:
    * `1` - analyze whole text file
    * `2` - analyze the first line of the file. If there is only one line (paragraph) it works the same as with `1` option
* `<finder_type>`:
    * `1` - count characters of the sentence (ignores spaces and tabs)
    * `2` - count words of the sentence
* `<output_type>`:
    * `1` - find and shows a sentence with the smallest count
    * `2` - find and shows a sentence (including the count) with the smallest count
    * `3` - find and shows a sentence with the highest count
    * `4` - find and shows a sentence (including the count) with the highest count
    * `5` - sort the sentences by the count in the ascending order
    * `6` - sort the sentences (including the count) by the count in the ascending order
    * `7` - sort the sentences by the count in the descending order
    * `8` - sort the sentences (including the count) by the count in the descending order

There is a possibility to add another Finder (implement `FinderInterface`) and Output (implement `OutputInterface`) classes to extend the functionality.

* The `Finder` and `Output` class must contain a `TYPE` constant with the unique number in a specific scope. This number must be added in the validation (`config/validations.php`) and the map (`condig/map.php`) configuration file;

### An example:
The command example: `php analyzer.php ./resources/texts/text_1.txt 1 1 8`

The results:
```
------ Results ------
138 | Morbi eget posuere urna, sed tincidunt laoreet magna, duis sollicitudin, ligula id vehicula dignissim, massa dolor imperdiet mauris, sed tempus felis ex ut orci.
----
88 | Etiam pharetra, felis ac aliquet pulvinar, mi ante ultricies lorem, vitae vehicula nisl dui non mauris.
----
81 | Cras porttitor, augue in viverra laoreet, neque leo congue mi, eu egestas metus sapien nec orci.
----
67 | Nunc egestas malesuada justo sed hendrerit, quisque vitae pellentesque enim.
----
63 | Cras ex est, ornare malesuada risus vitae, vestibulum scelerisque metus.
----
62 | Vivamus lacus metus, scelerisque in nulla id, dignissim venenatis nunc.
----
51 | Etiam finibus ullamcorper ex, et gravida erat vulputate et.
----
35 | Nunc non varius risus, non ultrices quam.
----
16 | Sed nec dictum est.
---------------------
```
