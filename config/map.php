<?php

use App\Finder\CharsCountFinder;
use App\Finder\WordsCountFinder;
use App\Output\MaxCountOutput;
use App\Output\MaxCountWithCountOutput;
use App\Output\MinCountOutput;
use App\Output\MinCountWithCountOutput;
use App\Output\SortAscOutput;
use App\Output\SortAscWithCountOutput;
use App\Output\SortDescOutput;
use App\Output\SortDescWithCountOutput;

return [
    'finder' => [
        CharsCountFinder::TYPE => CharsCountFinder::class,
        WordsCountFinder::TYPE => WordsCountFinder::class,
    ],
    'output' => [
        MaxCountOutput::TYPE          => MaxCountOutput::class,
        MaxCountWithCountOutput::TYPE => MaxCountWithCountOutput::class,
        MinCountOutput::TYPE          => MinCountOutput::class,
        MinCountWithCountOutput::TYPE => MinCountWithCountOutput::class,
        SortAscOutput::TYPE           => SortAscOutput::class,
        SortAscWithCountOutput::TYPE  => SortAscWithCountOutput::class,
        SortDescOutput::TYPE          => SortDescOutput::class,
        SortDescWithCountOutput::TYPE => SortDescWithCountOutput::class,
    ],
];
