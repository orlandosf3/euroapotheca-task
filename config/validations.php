<?php declare(strict_types=1);

use App\Finder\CharsCountFinder;
use App\Finder\WordsCountFinder;
use App\Output\MaxCountOutput;
use App\Output\MaxCountWithCountOutput;
use App\Output\MinCountOutput;
use App\Output\MinCountWithCountOutput;
use App\Output\SortAscOutput;
use App\Output\SortAscWithCountOutput;
use App\Output\SortDescOutput;
use App\Output\SortDescWithCountOutput;
use App\Service\SentenceParser;

return [
    1 => [
        'defined' => 'The File path is not defined',
        'path' => 'The File path is not valid'
    ],
    2 => [
        'defined' => 'The parser scope is not defined',
        'options' => [
            'in' => [
                SentenceParser::SCOPE_FILE,
                SentenceParser::SCOPE_LINE,
            ],
            'message' => 'Provided wrong scope',
        ],
    ],
    3 => [
        'defined' => 'The finder type is not defined',
        'options' => [
            'in'      => [
                CharsCountFinder::TYPE,
                WordsCountFinder::TYPE,
            ],
            'message' => 'Provided finder output type',
        ],
    ],
    4 => [
        'defined' => 'The output type is not defined',
        'options' => [
            'in'      => [
                MinCountOutput::TYPE,
                MinCountWithCountOutput::TYPE,
                MaxCountOutput::TYPE,
                MaxCountWithCountOutput::TYPE,
                SortAscOutput::TYPE,
                SortAscWithCountOutput::TYPE,
                SortDescOutput::TYPE,
                SortDescWithCountOutput::TYPE,
            ],
            'message' => 'Provided wrong output type',
        ],
    ],
];