<?php declare(strict_types=1);

namespace App\Tests\Unit;

use App\Factory\SentenceParserFactory;
use App\Service\SentenceParser;
use App\Service\SentenceParserInterface;
use PHPUnit\Framework\TestCase;

final class SentenceParserTest extends TestCase
{
    /**
     * @var SentenceParserInterface
     */
    private $parser;

    /**
     * @var string
     */
    private $dir;

    protected function setUp(): void
    {
        $this->parser = SentenceParserFactory::createNew(SentenceParser::class);
        $this->dir    = __DIR__ . '/../resources/texts/';
    }

    /**
     * @return array[]
     */
    public function resourcesProvider(): array
    {
        return [
            ['text_2_lines_9_only_dots.txt', '.?!', SentenceParser::SCOPE_FILE, true, 9],
            ['text_2_lines_9_only_dots.txt', '?!', SentenceParser::SCOPE_FILE, true, 2],
            ['text_empty.txt', '.?!', SentenceParser::SCOPE_FILE, false, 0],
            ['text_2_lines_9_dots_question_marks.txt', '.?!', SentenceParser::SCOPE_FILE, true, 9],
            ['text_2_lines_9_dots_question_marks.txt', '.!', SentenceParser::SCOPE_FILE, true, 8],
            ['text_1_line_9_only_dots.txt', '.?!', SentenceParser::SCOPE_FILE, true, 9],
            ['text_1_line_9_only_dots.txt', '?!', SentenceParser::SCOPE_FILE, true, 1],
            ['text_1_line_9_dots_question_marks.txt', '.?!', SentenceParser::SCOPE_FILE, true, 9],
            ['text_1_line_9_dots_question_marks.txt', '.!', SentenceParser::SCOPE_FILE, true, 7],

            ['text_2_lines_9_only_dots.txt', '.?!', SentenceParser::SCOPE_LINE, true, 2],
            ['text_2_lines_9_only_dots.txt', '?!', SentenceParser::SCOPE_LINE, true, 1],
            ['text_empty.txt', '.?!', SentenceParser::SCOPE_LINE, false, 0],
            ['text_2_lines_9_dots_question_marks.txt', '.?!', SentenceParser::SCOPE_LINE, true, 2],
            ['text_2_lines_9_dots_question_marks.txt', '.!', SentenceParser::SCOPE_LINE, true, 2],
            ['text_1_line_9_only_dots.txt', '.?!', SentenceParser::SCOPE_LINE, true, 9],
            ['text_1_line_9_only_dots.txt', '?!', SentenceParser::SCOPE_LINE, true, 1],
            ['text_1_line_9_dots_question_marks.txt', '.?!', SentenceParser::SCOPE_LINE, true, 9],
            ['text_1_line_9_dots_question_marks.txt', '.!', SentenceParser::SCOPE_LINE, true, 7],
        ];
    }

    /**
     * @dataProvider resourcesProvider
     *
     * @param string $file
     * @param string $terminators
     * @param int    $scope
     * @param bool   $isNotEmpty
     * @param int    $count
     */
    public function testTextHasSentences(string $file, string $terminators, int $scope, bool $isNotEmpty, int $count): void
    {
        $sentenceSet = $this->parser->parse($this->dir . $file, $terminators, $scope);

        $this->assertEquals($isNotEmpty, $sentenceSet->isNotEmpty());
        $this->assertEquals($count, $sentenceSet->count());
    }
}
