<?php declare(strict_types=1);

namespace App\Tests\Unit;

use App\DataSet\SentenceSet;
use App\Factory\AnalyzerFactory;
use App\Factory\FinderFactory;
use App\Factory\OutputFactory;
use App\Finder\CharsCountFinder;
use App\Finder\WordsCountFinder;
use App\Output\MaxCountOutput;
use App\Output\MaxCountWithCountOutput;
use App\Output\MinCountOutput;
use App\Output\MinCountWithCountOutput;
use App\Output\SortAscOutput;
use App\Output\SortAscWithCountOutput;
use App\Output\SortDescOutput;
use App\Output\SortDescWithCountOutput;
use App\Service\Analyzer;
use App\Service\AnalyzerInterface;
use PHPUnit\Framework\TestCase;

class AnalyzerTest extends TestCase
{
    /**
     * @var AnalyzerInterface
     */
    private $analyzer;

    protected function setUp(): void
    {
        $this->analyzer = AnalyzerFactory::createNew(Analyzer::class);
    }

    public function dataProvider(): array
    {
        $sentences1 = [
            'Morbi eget posuere urna, sed tincidunt laoreet magna, duis sollicitudin,    ligula id vehicula dignissim, massa dolor imperdiet mauris, sed tempus felis ex ut orci.',
            'Nunc egestas malesuada justo sed hendrerit, quisque vitae pellentesque enim?',
            'Cras ex est, ornare malesuada risus vitae, vestibulum scelerisque metus.',
            'Vivamus lacus metus, scelerisque in nulla id, dignissim venenatis nunc.',
            'Etiam finibus ullamcorper ex, et gravida erat vulputate et.',
            'Nunc non varius risus, non ultrices quam.',
            'Sed nec dictum est.',
            'Etiam pharetra, felis ac aliquet pulvinar, mi ante ultricies lorem, vitae vehicula nisl dui non mauris?',
            'Cras porttitor, augue in viverra laoreet, neque leo congue mi, eu egestas metus sapien nec orci.',
            'Ab bc cd de ef.',
        ];

        return [
            [$sentences1, CharsCountFinder::class, MinCountOutput::class, 1, ['Ab bc cd de ef.']],
            [$sentences1, CharsCountFinder::class, MinCountWithCountOutput::class, 1, ['11 | Ab bc cd de ef.']],
            [$sentences1, CharsCountFinder::class, MaxCountOutput::class, 1, ['Morbi eget posuere urna, sed tincidunt laoreet magna, duis sollicitudin,    ligula id vehicula dignissim, massa dolor imperdiet mauris, sed tempus felis ex ut orci.']],
            [$sentences1, WordsCountFinder::class, MinCountWithCountOutput::class, 1, ['4 | Sed nec dictum est.']],
            [$sentences1, WordsCountFinder::class, MaxCountWithCountOutput::class, 1, ['24 | Morbi eget posuere urna, sed tincidunt laoreet magna, duis sollicitudin,    ligula id vehicula dignissim, massa dolor imperdiet mauris, sed tempus felis ex ut orci.']],
            [$sentences1, CharsCountFinder::class, SortAscOutput::class, 10, [
                'Ab bc cd de ef.',
                'Sed nec dictum est.',
                'Nunc non varius risus, non ultrices quam.',
                'Etiam finibus ullamcorper ex, et gravida erat vulputate et.',
                'Vivamus lacus metus, scelerisque in nulla id, dignissim venenatis nunc.',
                'Cras ex est, ornare malesuada risus vitae, vestibulum scelerisque metus.',
                'Nunc egestas malesuada justo sed hendrerit, quisque vitae pellentesque enim?',
                'Cras porttitor, augue in viverra laoreet, neque leo congue mi, eu egestas metus sapien nec orci.',
                'Etiam pharetra, felis ac aliquet pulvinar, mi ante ultricies lorem, vitae vehicula nisl dui non mauris?',
                'Morbi eget posuere urna, sed tincidunt laoreet magna, duis sollicitudin,    ligula id vehicula dignissim, massa dolor imperdiet mauris, sed tempus felis ex ut orci.',
            ]],
            [$sentences1, CharsCountFinder::class, SortDescOutput::class, 10, [
                'Morbi eget posuere urna, sed tincidunt laoreet magna, duis sollicitudin,    ligula id vehicula dignissim, massa dolor imperdiet mauris, sed tempus felis ex ut orci.',
                'Etiam pharetra, felis ac aliquet pulvinar, mi ante ultricies lorem, vitae vehicula nisl dui non mauris?',
                'Cras porttitor, augue in viverra laoreet, neque leo congue mi, eu egestas metus sapien nec orci.',
                'Nunc egestas malesuada justo sed hendrerit, quisque vitae pellentesque enim?',
                'Cras ex est, ornare malesuada risus vitae, vestibulum scelerisque metus.',
                'Vivamus lacus metus, scelerisque in nulla id, dignissim venenatis nunc.',
                'Etiam finibus ullamcorper ex, et gravida erat vulputate et.',
                'Nunc non varius risus, non ultrices quam.',
                'Sed nec dictum est.',
                'Ab bc cd de ef.',
            ]],
            [$sentences1, CharsCountFinder::class, SortAscWithCountOutput::class, 10, [
                '11 | Ab bc cd de ef.',
                '16 | Sed nec dictum est.',
                '35 | Nunc non varius risus, non ultrices quam.',
                '51 | Etiam finibus ullamcorper ex, et gravida erat vulputate et.',
                '62 | Vivamus lacus metus, scelerisque in nulla id, dignissim venenatis nunc.',
                '63 | Cras ex est, ornare malesuada risus vitae, vestibulum scelerisque metus.',
                '67 | Nunc egestas malesuada justo sed hendrerit, quisque vitae pellentesque enim?',
                '81 | Cras porttitor, augue in viverra laoreet, neque leo congue mi, eu egestas metus sapien nec orci.',
                '88 | Etiam pharetra, felis ac aliquet pulvinar, mi ante ultricies lorem, vitae vehicula nisl dui non mauris?',
                '138 | Morbi eget posuere urna, sed tincidunt laoreet magna, duis sollicitudin,    ligula id vehicula dignissim, massa dolor imperdiet mauris, sed tempus felis ex ut orci.',
            ]],
            [$sentences1, CharsCountFinder::class, SortDescWithCountOutput::class, 10, [
                '138 | Morbi eget posuere urna, sed tincidunt laoreet magna, duis sollicitudin,    ligula id vehicula dignissim, massa dolor imperdiet mauris, sed tempus felis ex ut orci.',
                '88 | Etiam pharetra, felis ac aliquet pulvinar, mi ante ultricies lorem, vitae vehicula nisl dui non mauris?',
                '81 | Cras porttitor, augue in viverra laoreet, neque leo congue mi, eu egestas metus sapien nec orci.',
                '67 | Nunc egestas malesuada justo sed hendrerit, quisque vitae pellentesque enim?',
                '63 | Cras ex est, ornare malesuada risus vitae, vestibulum scelerisque metus.',
                '62 | Vivamus lacus metus, scelerisque in nulla id, dignissim venenatis nunc.',
                '51 | Etiam finibus ullamcorper ex, et gravida erat vulputate et.',
                '35 | Nunc non varius risus, non ultrices quam.',
                '16 | Sed nec dictum est.',
                '11 | Ab bc cd de ef.',
            ]],
        ];
    }

    /**
     * @dataProvider dataProvider
     *
     * @param array  $sentences
     * @param string $finderClass
     * @param string $outputClass
     * @param int    $count
     * @param array  $value
     */
    public function testAnalyzer(array $sentences, string $finderClass, string $outputClass, int $count, array $value)
    {
        $sentenceSet = new SentenceSet();
        $sentenceSet->setSentences($sentences);

        $output = $this->analyzer->analyze(
            $sentenceSet,
            FinderFactory::createNew($finderClass),
            OutputFactory::createNew($outputClass)
        );

        $this->assertCount($count, $output);
        $this->assertEquals($value, $output);
    }
}
